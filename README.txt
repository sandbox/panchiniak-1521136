README.txt
==========
Objective
=========
Tefiltro improves the usability of your site through a process of term
filtering. It hides from the "options list" of a "term reference" field, those
options the visitor will not be able to choose under certain scenarios (Vide
Scenario bellow).
It hides the options that represent terms not referenced at nodes authored by:
    current user OR
    members of the oldest organic group owned by current user.

SCENARIO
========
Visitor wants to choose driving (node drive) some of his several cars (node car)
according to their colors (vocabulary color). Each car has a term reference to a
color, but not all colors stored in the vocabulary represent the cars owned by
Visitor. He would like to filter the select options list to the colors of his
own cars, or to the colors of the cars of his friends.

USAGE
=====
Tefiltro filters the options list of a term reference filed in a node add/edit
form. Configure admin/config/content/tefiltro. Fill the form with:
    Target Field,
    Origin Field and
    Target Content Type.
    
Note: Target Field is the field used to select previsouly created terms, while
Origin Field is the term reference field used to create terms that will be then
selected by Target Field.

STEP-BY-STEP
============
Example from a new Drupal 7 installation:

1. Install and enable tefiltro and noderefcreate (the latter is only necessary
for this example)
Type: drush dl tefiltro noderefcreate; drush en tefiltro noderefcreate

2. Create the taxonomy vocabulary "color".
Go to: admin/structure/taxonomy/add

4. Create the content types "car" and "drive".
Go to: admin/structure/types/add

5. Create into "car" the term reference field "carcolor" pointing to "color".
Go to: admin/structure/types/manage/car/fields
Note: Use autocomplete widget. Users will be able to add new colors from this
content.

6. Create into "drive" the term reference field "bycolor" pointing to "color".
Go to: admin/structure/types/manage/drive/fields
Note: Use select list widget. Users will be able to choose a color from this
content.

7. Configure Tefiltro: Target Field: "bycolor"; Origin Field: "carcolor";
Target Content Type: "drive".
Go to: admin/config/content/tefiltro

User will see in the options list of "bycolor" only the colors referenced by
his/her own cars or by the the cars of his/her friends.

AUTHOR
======
Rodrigo Panchiniak Fernandes <fernandesrp at cce DOT ufsc DOT br>
http://www.cce.ufsc.br/~fernandesrp
